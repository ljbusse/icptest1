#ifndef NMSBOX_H
#define NMSBOX_H

#include "hidapi.h"
#define MAX_STR 65

class NMSBox
{
public:
    NMSBox();
    ~NMSBox();

    void ReadWriteLoop();
    short int EchoY[4000];
    bool AttachedState;


private:
    int res;
 //   unsigned char buf[256];
    wchar_t wstr[MAX_STR];
    hid_device *handle;

    struct hid_device_info *devs, *cur_dev;
};

#endif // NMSBOX_H
