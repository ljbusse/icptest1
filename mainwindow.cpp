#include "mainwindow.h"
#include "nmsbox.h"
#include "fannclass.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupRealtimeDataDemo(ui->customPlot);
    statusBar()->clearMessage();
    currentDemoIndex = 10;
    ui->customPlot->replot();
    box = new NMSBox();
//    if (box->AttachedState == false){
//        QErrorMessage("Could not connect to NMS Box");
//        exit;
//    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupRealtimeDataDemo(QCustomPlot *customPlot)
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif
  demoName = "Real Time Data";
  ann = new fannClass();
  // include this section to fully disable antialiasing for higher performance:

  customPlot->setNotAntialiasedElements(QCP::aeAll);
  QFont font;
  font.setStyleStrategy(QFont::NoAntialias);
  customPlot->xAxis->setTickLabelFont(font);
  customPlot->yAxis->setTickLabelFont(font);
  customPlot->legend->setFont(font);
  //
  customPlot->addGraph(); // blue line
  customPlot->graph(0)->setPen(QPen(Qt::blue));
//  customPlot->graph(0)->setBrush(QBrush(QColor(240, 255, 200)));
  customPlot->graph(0)->setAntialiasedFill(false);

  customPlot->xAxis->setRange(0.,175.);
  customPlot->xAxis->setAutoTickStep(25);
  customPlot->xAxis->setTickStep(25);
  customPlot->axisRect()->setupFullAxesBox();

  // make left and bottom axes transfer their ranges to right and top axes:
  connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
  connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

  // setup a timer that repeatedly calls MainWindow::g:
  connect(&dataTimer, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
  dataTimer.start(10); // Interval 0 means to refresh as fast as possible
}

void MainWindow::realtimeDataSlot()
{
  // calculate two new data points:
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  double key = 0;
#else
  double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
#endif
  char sss[10];
  static int count =0;
  static double lastPointKey = 0;
  static QVector<double> xval(175), yval(175);
  if (key-lastPointKey > 0.1) // at most add point every 50 ms
  {
    box->ReadWriteLoop();
    for (int i = 0; i<175;i++){
        xval[i] = (double)i;
//        yval[i] = (double)qrand();
        yval[i] = (double)box->EchoY[i];
    }
    count++;
    if (count%10 == 0)
        ann->AddWave(box->EchoY);

    sprintf(sss,"%3.1f",ann->ICP);

    if (ann->valid)
        ui->lcdNumber->display(sss);
    else
        ui->lcdNumber->display("0.0");

    ui->customPlot->graph(0)->setData(xval, yval);  //addData(key, value0);
    lastPointKey = key;
  }

  ui->customPlot->xAxis->setTickStep(25);
  ui->customPlot->yAxis->setRange(8240-25,8240+25);
  ui->customPlot->replot();

  /*
  // calculate frames per second:
  static double lastFpsKey;
  static int frameCount;
  ++frameCount;
  if (key-lastFpsKey > 2) // average fps over 2 seconds
  {
    ui->statusBar->showMessage(
          QString("%1 FPS, Total Data points: %2")
          .arg(frameCount/(key-lastFpsKey), 0, 'f', 0)
          .arg(ui->customPlot->graph(0)->data()->count())
          , 0);
    lastFpsKey = key;
    frameCount = 0;
  }
  */
}
