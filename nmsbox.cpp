#include <stdio.h>
#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "nmsbox.h"

NMSBox::NMSBox()
{
    AttachedState = false;

    //    if (hid_init())
    //        throw int();   hid_init();

    devs = hid_enumerate(0x0, 0x0);
    cur_dev = devs;
    hid_free_enumeration(devs);

    // Set up the command buffer.
//	memset(buf,0x00,sizeof(buf));
//	buf[0] = 0x01;
//	buf[1] = 0x81;


    // Open the device using the VID, PID,
    // and optionally the Serial number.
    handle = hid_open(0x04d8, 0x004f, L"");	//LJB
    if (!handle) {
        printf("unable to open NMS device\n");
//        return 1;
    } else {
        AttachedState = true;
    }

    if (0) {
        // Read the Manufacturer String
        wstr[0] = 0x0000;
        res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read manufacturer string\n");
        printf("Manufacturer String: %ls\n", wstr);

        // Read the Product String
        wstr[0] = 0x0000;
        res = hid_get_product_string(handle, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read product string\n");
        printf("Product String: %ls\n", wstr);

        // Read the Serial Number String
        wstr[0] = 0x0000;
        res = hid_get_serial_number_string(handle, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read serial number string\n");
        printf("Serial Number String: (%d) %ls", wstr[0], wstr);
        printf("\n");

        // Read Indexed String 1
        wstr[0] = 0x0000;
        res = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
        if (res < 0)
            printf("Unable to read indexed string 1\n");
        printf("Indexed String 1: %ls\n", wstr);
    }
}

NMSBox::~NMSBox(){
    hid_close(handle);
    /* Free static HIDAPI objects. */
    hid_exit();
}

//typedef unsigned int DWORD ;

void NMSBox::ReadWriteLoop(){
    unsigned char OUTBuffer[65];	//Allocate a memory buffer equal to the OUT endpoint size + 1
    unsigned char INBuffer[65];		//Allocate a memory buffer equal to the IN endpoint size + 1
//    int iirange;
    bool ToggleLEDsPending,PushbuttonPressed;
    bool Dspicdata[10];
    int res;
    int irange=0;
    unsigned int  DspicCommand[2]={0, 0};
    int count =0;
    FILE *fp;

    fp = fopen("temp.bin","wb");
    if (fp == 0){
        printf("Problem opening file\n");
        return;
    }
    hid_set_nonblocking(handle, 0);	//disable non-blocking == blocking?

    {ToggleLEDsPending= true;}
//		while(true)
    while(count < 1){
        if(true)
            {ToggleLEDsPending=true;}
        if(AttachedState == true)	//Do not try to use the read/write handles unless the USB device is attached and ready
        {
            //Get ANxx/POT Voltage value from the microcontroller firmware.  Note: some demo boards may not have a pot
            //on them.  In this case, the firmware may be configured to read an ANxx I/O pin voltage with the ADC
            //instead.  If this is the case, apply a proper voltage to the pin.  See the firmware for exact implementation.
            OUTBuffer[0] = 0;			//The first byte is the "Report ID" and does not get sent over the USB bus.  Always set = 0.
            OUTBuffer[1] = 0x37;	//READ_POT command (see the firmware source code), gets 10-bit ADC Value
            //Initialize the rest of the 64-byte packet to "0xFF".  Binary '1' bits do not use as much power, and do not cause as much EMI
            //when they move across the USB cable.  USB traffic is "NRZI" encoded, where '1' bits do not cause the D+/D- signals to toggle states.
            //This initialization is not strictly necessary however.
            for(unsigned char i = 2; i <65; i++)
                OUTBuffer[i] = 0xFF;

            //To get the ADCValue, first, we send a packet with our "READ_POT" command in it.
            res = hid_write(handle, OUTBuffer, 65);
            if (res >= 0){
                INBuffer[0] = 0;
                //Now get the response packet from the firmware.
                res = hid_read(handle, INBuffer, 65);

                //LJB for some reason the reads only seem to be returning 64 bytes so I shifted all indices DOWN by 1.
                if (res == 64){
                    if(INBuffer[0] == 0x37)
                    {
                        int ADCValue = (INBuffer[2] << 8) + INBuffer[1]; // vitor	//Need to reformat the data from two unsigned chars into one unsigned int.
                    }

                }
            }



            //Get the pushbutton state from the microcontroller firmware.
            OUTBuffer[0] = 0;			//The first byte is the "Report ID" and does not get sent over the USB bus.  Always set = 0.
            OUTBuffer[1] = 0x81;		//0x81 is the "Get Pushbutton State" command in the firmware
            for(unsigned char i = 2; i <65; i++)	//This loop is not strictly necessary.  Simply initializes unused bytes to
                OUTBuffer[i] = 0xFF;		// vitor		//0xFF for lower EMI and power consumption when driving the USB cable.

            //To get the pushbutton state, first, we send a packet with our "Get Pushbutton State" command in it.
            res = hid_write(handle, OUTBuffer, 65);
            if (res >= 0){
                //Now get the response packet from the firmware.
                INBuffer[0] = 0;
                res = hid_read(handle, INBuffer, 65);
                //LJB for some reason the reads only seem to be returning 64 bytes so I shifted all indices DOWN by 1.
                if (res == 64){
                    if((INBuffer[0] == 0x81) && (INBuffer[1] == 0x01))
                    {// vitor
                        PushbuttonPressed = false;
                    }
                    if((INBuffer[0] == 0x81) && (INBuffer[1] == 0x00))
                    {
                        PushbuttonPressed = true;
                    }
                    if(INBuffer[0] == 0x81)
                    {
                        if(INBuffer[1]==0)Dspicdata[0]=false;
                        else Dspicdata[0]=true;
                        if(INBuffer[2]==0)Dspicdata[1]=false;
                        else Dspicdata[1]=true;
                        if(INBuffer[3]==0)Dspicdata[2]=false;
                        else Dspicdata[2]=true;

                        int ipackt= 25;
                        int ipack,packb=3;//,packbs=25;

 //                       int zera=0;

                        if((INBuffer[2]+INBuffer[3]*256)==0) {
                            irange=0;
                        }
 //                       iirange=irange;
                        for(ipack=irange*ipackt;ipack<ipackt+ipackt*irange;ipack=ipack+1)
                        {
                                EchoY[ipack]=(INBuffer[packb-1]+INBuffer[packb]*256);
                                packb=packb+2;
                        }
                        irange=irange+1;
                        if(irange > 6 ){
                            fwrite( &EchoY, sizeof(short),175,fp);
                            irange=0;
                            count += 1;
                        }

                    }
                }
            }

            //Check if this thread should send a Toggle LED(s) command to the firmware.  ToggleLEDsPending will get set
            //by the ToggleLEDs_btn click event handler function if the user presses the button on the form.
            if(ToggleLEDsPending == true)
            {
                OUTBuffer[0] = 0;					//The first byte is the "Report ID" and does not get sent over the USB bus.  Always set = 0.
                OUTBuffer[1] = 0x80;//0x80 is the "Toggle LED(s)" command in the firmware
                OUTBuffer[2] =DspicCommand[0];   //0x10; //aqui
                OUTBuffer[3] =DspicCommand[1]; //0x01;
                DspicCommand[0]=0;
                DspicCommand[1]=0;
                for(unsigned char i = 4; i <65; i++)	//This loop is not strictly necessary.  Simply initializes unused bytes to
                    OUTBuffer[i] = 0xFF;				//0xFF for lower EMI and power consumption when driving the USB cable.
                //Now send the packet to the USB firmware on the microcontroller
                res = hid_write(handle, OUTBuffer, 65);
                ToggleLEDsPending = false;
            }
        } //end of: if(AttachedState == true)
        else
        {
            usleep(50000);	//Add a small delay.  Otherwise, this while(true) loop can execute very fast and cause
                        //high CPU utilization, with no particular benefit to the application.
        }

    } //end of while(true) loop
    fclose(fp);
}
