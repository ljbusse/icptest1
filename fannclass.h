#ifndef FANNCLASS_H
#define FANNCLASS_H
#include "fann.h"

class fannClass
{
public:
    fannClass();
    ~fannClass();
    void NormalizeData();
    void AddWave(short int *data);
    int count;
    int waves[10][175];
    int rawdata[870];
    fann_type normData[870];
    fann_type ICP;
    bool valid;

private:
    struct fann *ann;
    fann_type res[10];


};

#endif // FANNCLASS_H
