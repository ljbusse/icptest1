#-------------------------------------------------
#
# Project created by QtCreator 2016-05-20T20:20:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = ICPtest1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    nmsbox.cpp \
    hid.c \
    ../../FANN-2.2.0-Source/src/floatfann.c \
    fannclass.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    nmsbox.h \
    hidapi.h \
    ../../FANN-2.2.0-Source/src/include/compat_time.h \
    ../../FANN-2.2.0-Source/src/include/config.h \
 #   ../../FANN-2.2.0-Source/src/include/doublefann.h \
    ../../FANN-2.2.0-Source/src/include/fann.h \
    ../../FANN-2.2.0-Source/src/include/fann_activation.h \
    ../../FANN-2.2.0-Source/src/include/fann_cascade.h \
    ../../FANN-2.2.0-Source/src/include/fann_cpp.h \
    ../../FANN-2.2.0-Source/src/include/fann_data.h \
    ../../FANN-2.2.0-Source/src/include/fann_error.h \
    ../../FANN-2.2.0-Source/src/include/fann_internal.h \
    ../../FANN-2.2.0-Source/src/include/fann_io.h \
    ../../FANN-2.2.0-Source/src/include/fann_train.h \
    ../../FANN-2.2.0-Source/src/include/fixedfann.h \
    ../../FANN-2.2.0-Source/src/include/floatfann.h \
    fannclass.h

INCLUDEPATH += ../../FANN-2.2.0-Source/src/include

FORMS    += mainwindow.ui

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libudev


unix: PKGCONFIG += libusb-1.0
