#include "fannclass.h"
#include <QDebug>

fannClass::fannClass()
{
    ann = fann_create_from_file("multipleFrames-6-10.net");
    count = 0;
    ICP = 0.0;
    valid = false;
}

fannClass::~fannClass()
{
    fann_destroy(ann);
}


void fannClass::AddWave(short int *data){
    int i,j,index;
    for (i = 1; i<175; i++)
        waves[count][i] = data[i];
    count+=1;

    if (count == 10){
        //Resample take every other point
        index = 0;
        for (i=0; i<10; i++){
            for (j=1; j<175; j+=2){
                rawdata[index++] = waves[i][j];
            }
        }
        count = 0;
        NormalizeData();
        fann_type *ptr = new fann_type [10];
        ptr = fann_run(ann, normData);
        ICP = ptr[0];
        //delete[] ptr;
    }
}

#define ABS(a) ((a)<0.0 ? (-a) : (a))
void fannClass::NormalizeData(){
    static fann_type MAX=8500.;
    static fann_type MIN=8000.;
    int i;

    static fann_type OFFSET1=8242;//(MAX + MIN) / (fann_type)2.0;
    static fann_type OFFSET2=(MAX - MIN) / (fann_type)2.0;

    fann_type value;

    //remove DC-bias from signals
    float sum=0.0;
    for (i=0; i<870; i++)
        sum += (float)rawdata[i];
    float offset1 = sum / (float)870;

    //Now do scaling
    sum = 0.0;
    for (i = 0; i < 870; i++) {
        value = (fann_type)rawdata[i];
//        value = (value-OFFSET1)/OFFSET2;
        value = (value-offset1)/OFFSET2;
        if (value < -1.) {
            qDebug("value %f too small at i= %d\n", value, i);
            value = -1.0;
        }

        if (value >  1.) {
            qDebug("value %f too large at i= %d\n", value, i);
            value = +1.0;
        }
        normData[i] = value;
        sum+=ABS(value);


    }
    valid=false;
    if (sum > 5.)       //5. is arbitrary number from trial and error
        valid=true;
    qDebug("sum = %f",sum);
}
